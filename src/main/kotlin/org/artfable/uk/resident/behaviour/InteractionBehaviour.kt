package org.artfable.uk.resident.behaviour

import org.artfable.telegram.api.Behaviour
import org.artfable.telegram.api.Message
import org.artfable.telegram.api.Update
import org.artfable.telegram.api.keyboard.InlineKeyboard
import org.artfable.telegram.api.keyboard.InlineKeyboardBtn
import org.artfable.telegram.api.request.DeleteMessageRequest
import org.artfable.telegram.api.request.SendMessageRequest
import org.artfable.telegram.api.service.TelegramSender
import org.artfable.uk.resident.model.Answer
import org.artfable.uk.resident.model.Question
import org.artfable.uk.resident.service.QuestionService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class InteractionBehaviour: Behaviour {

    companion object {
        private val logger: Logger = LoggerFactory.getLogger(InteractionBehaviour::class.java)
    }

    @Autowired
    private lateinit var telegramSender: TelegramSender

    @Autowired
    private lateinit var questionService: QuestionService

    override fun parse(update: Update) {
        logger.trace(update.toString())
        when {
            update.message?.text?.startsWith("/start") == true -> {
                questionService.nextQuestion(QuestionService.START_CODE)
            }
            update.callbackQuery?.data != null -> {
                questionService.nextQuestion(update.callbackQuery!!.data!!)
            }
            else -> null
        }?.let { nextQuestion ->
            this.sendQuestion(update, nextQuestion)
        }
    }

    private fun sendQuestion(update: Update, question: Question) {
        val message = update.extractMessage()!!

        telegramSender.singleExecuteMethod<Boolean>(update.updateId, DeleteMessageRequest(message.chat.id, message.messageId))
            ?.let {
                telegramSender.executeMethod<Message>(
                    SendMessageRequest(
                        message.chat.id,
                        question.text,
                        replyMarkup = answersToInlineKeyboard(question.answers)
                    )
                )?.let { logger.debug(it.toString()) }
            }
    }

    private fun answersToInlineKeyboard(answers: List<Answer>): InlineKeyboard {
        return InlineKeyboard(*answers
            .map { InlineKeyboardBtn(it.text, it.code) }
            .toTypedArray())
    }
}