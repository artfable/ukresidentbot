package org.artfable.uk.resident

import org.artfable.telegram.api.LongPollingTelegramBot
import org.artfable.telegram.api.WebhookTelegramBot
import org.artfable.uk.resident.behaviour.InteractionBehaviour
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Service

@Service
class UKResidentBot @Autowired constructor(
    @Value("\${telegram.bot.token}") token: String,
    @Value("\${telegram.bot.webhook.url}") url: String,
    @Value("\${telegram.bot.key.public}") cert: Resource,
    interactionBehaviour: InteractionBehaviour
)
//    : WebhookTelegramBot(url, cert, setOf(interactionBehaviour), setOf())
    : LongPollingTelegramBot(setOf(interactionBehaviour), setOf())