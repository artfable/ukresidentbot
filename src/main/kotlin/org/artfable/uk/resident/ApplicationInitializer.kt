package org.artfable.uk.resident

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.PropertySource
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
@PropertySource("classpath:bot-secret.properties")
class ApplicationInitializer

fun main(args: Array<String>) {
    SpringApplication.run(ApplicationInitializer::class.java, *args)
}