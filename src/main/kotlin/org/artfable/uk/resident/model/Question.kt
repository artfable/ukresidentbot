package org.artfable.uk.resident.model

data class Question(
    val text: String,
    val answers: List<Answer> = emptyList()
)