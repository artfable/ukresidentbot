package org.artfable.uk.resident.model

data class Answer(
    val code: String,
    val text: String
)