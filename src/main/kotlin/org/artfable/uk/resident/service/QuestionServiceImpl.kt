package org.artfable.uk.resident.service

import org.artfable.uk.resident.model.Answer
import org.artfable.uk.resident.model.Question
import org.springframework.stereotype.Service

@Service
class QuestionServiceImpl : QuestionService {
    private val questions: Map<String, Question> = mapOf(
        Pair(
            "q1", Question(
                "Did you work full time abroad in the tax year?",
                listOf(Answer("q2", "Yes"), Answer("q0", "No"))
            )
        ),
        Pair(
            "q2", Question(
                "Did you work in the UK during the tax year?",
                listOf(Answer("q3", "Yes"), Answer("91d", "No"))
            )
        ),
        Pair(
            "q3", Question(
                "Was the number of working days in the UK with more than 3 hr workday LESS then 31?",
                listOf(Answer("91d", "Yes"), Answer("q0", "No"))
            )
        ),
        Pair(
            "q0", Question(
                "Where you tax resident of the UK in the preceding 3 years?",
                listOf(Answer("16d", "Yes"), Answer("46d", "No"))
            )
        ),
        // home (b2)
        Pair(
            "b2q0", Question(
                "Do you have any home (owned or rented) in the UK?",
                listOf(Answer("b2q1", "Yes"), Answer("b3q0", "No"))
            )
        ),
        Pair(
            "b2q1", Question(
                "Were you present in any of this homes at least 30 days during the tax year?",
                listOf(Answer("b2q4", "Yes"), Answer("b3q0", "No"))
            )
        ),
        Pair(
            "b2q4", Question(
                "Do you have any home outside of the UK?",
                listOf(Answer("b2q5", "Yes"), Answer("q1", "No"))
            )
        ),
        Pair(
            "b2q5", Question(
                "Were you present in any of this homes at least 30 days during the tax year?",
                listOf(Answer("b3q0", "Yes"), Answer("q1", "No"))
            )
        ),
        // worked in the UK (b3)
        Pair(
            "b3q0", Question(
                "Have ever you worked in the UK?",
                listOf(Answer("b3q1", "Yes"), Answer("b4q0", "No"))
            )
        ),
        Pair(
            "b3q1", Question(
                "Were working in the UK for more than 365 (all time)?",
                listOf(Answer("b3q2", "Yes"), Answer("b4q0", "No"))
            )
        ),
        Pair(
            "b3q2", Question(
                "Did you have a job on board a vehicle, aircraft or ship at any time in the tax year?",
                listOf(Answer("b3q3", "Yes"), Answer("b3a1q0", "No"))
            )
        ),
        Pair(
            "b3q3", Question(
                "Did you have at least 6 of the cross-boarder trips in the tax year as part of that job that begins or/and ends in the UK?",
                listOf(Answer("b4q0", "Yes"), Answer("b3a1q0", "No"))
            )
        ),
        Pair("b3a1q0", Question("А хуй его знает, где-то между 16 и 183 дней")),
        // past resident (b4)
        Pair(
            "b4q0", Question(
                "Were resident for preceding 3 years?",
                listOf(Answer("b4q1", "Yes"), Answer("b4q2", "No"))
            )
        ),
        Pair(
            "b4q1", Question(
                """How many of the following is true:
                    * Have a family with UK resident
                    * Have a UK place to live
                    * Have a work in the UK
                    * Have spent 90 days in previous tax year OR in tax year before previous
                    * Spent the most of your days in the UK
                """,
                listOf(Answer("183d", "0"), Answer("120d", "1"), Answer("q1", "2 or more"))
            )
        ),
        Pair(
            "b4q2", Question(
                """How many of the following is true:
                    * Have a family with UK resident
                    * Have a UK place to live
                    * Have a work in the UK
                    * Have spent 90 days in previous tax year OR in tax year before previous
                """,
                listOf(Answer("183d", "1 or less"), Answer("120d", "2"), Answer("q1", "3 or more"))
            )
        ),
        Pair("16d", Question("You became a resident after 16 days")),
        Pair("46d", Question("You became a resident after 46 days")),
        Pair("91d", Question("You became a resident after 91 days")),
        Pair("120d", Question("You became a resident after 120 days")),
        Pair("183d", Question("You became a resident after 183 days"))
    )

    override fun nextQuestion(answerCode: String): Question {
        val question = questions[answerCode]
        return question ?: throw IllegalArgumentException("No question for $answerCode")
    }


}