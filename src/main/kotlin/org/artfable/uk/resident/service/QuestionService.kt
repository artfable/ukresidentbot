package org.artfable.uk.resident.service

import org.artfable.uk.resident.model.Question

interface QuestionService {
    companion object {
        const val START_CODE: String = "b2q0"
    }

    fun nextQuestion(answerCode: String): Question
}