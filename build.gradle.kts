plugins {
    kotlin("jvm") version "1.4.0"
    id("org.jetbrains.kotlin.plugin.spring") version "1.4.0"
    id("org.springframework.boot") version "2.3.2.RELEASE"
}

group = "org.artfable"
version = "1.0-SNAPSHOT"

repositories {
    mavenLocal()
    jcenter()
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.springframework.boot:spring-boot-starter-web:2.3.2.RELEASE")
    implementation("org.artfable:telegram-api:0.4.0")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "11"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "11"
    }
}